const { Model, DataTypes } = require('sequelize');

class User extends Model {
  static init(sequelize) {
    return super.init({
      name: DataTypes.STRING
    }, {
      sequelize,
      modelName: 'user',
      tableName: 'users',
      freezeTableName: true
    });
  }
}

module.exports = User;
