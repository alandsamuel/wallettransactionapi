const Sequelize = require('sequelize');

const config = require('../../config');

const {
  db: {
    name,
    username,
    password,
    ...otherConfig
  }
} = config;

let sequelize = null;

const connect = () => {
  if (!sequelize) {
    sequelize = new Sequelize(name, username, password, otherConfig);
  }

  return sequelize;
};

const disconnect = () => sequelize.close();

module.exports = {
  connect,
  disconnect
};
