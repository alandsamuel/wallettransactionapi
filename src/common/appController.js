const express = require('express');
const swaggerUi = require('swagger-ui-express');

class AppController {
  constructor(app) {
    this._app = app;
    this._router = express.Router();
  }

  registerRoutes() {
    this._app.use('/docs', swaggerUi.serve, swaggerUi.setup());
  }
}

module.exports = AppController;
