const { Model, DataTypes } = require('sequelize');

class Transaction extends Model {
  static init(sequelize) {
    return super.init({
      walletId: {
        type: DataTypes.INTEGER,
        field: 'wallet_id'
      },
      amount: DataTypes.INTEGER,
      type: DataTypes.STRING,
      description: DataTypes.STRING
    }, {
      sequelize,
      modelName: 'transaction',
      tableName: 'transactions',
      freezeTableName: true
    });
  }

  static associate(models) {
    const { Transaction: TransactionModel, Wallet } = models;
    TransactionModel.belongsTo(Wallet, {
      foreignKey: 'walletId'
    });
  }
}

module.exports = Transaction;
