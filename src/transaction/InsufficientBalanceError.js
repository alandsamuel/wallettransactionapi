const createError = require('http-errors');

const { Forbidden } = createError;

class InsufficientBalanceError extends Forbidden {
  constructor() {
    super('Insufficient balance');
  }
}

module.exports = InsufficientBalanceError;
