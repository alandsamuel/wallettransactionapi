const Joi = require('@hapi/joi');

const post = {
  body: Joi.object({
    walletId: Joi.number().integer().min(1).required(),
    amount: Joi.number().integer().min(0).required(),
    type: Joi.string().valid('TOP UP', 'WITHDRAW').required(),
    description: Joi.string()
  })
};

module.exports = {
  post
};
