// const BookNotFoundError = require('../book/BookNotFoundError');
// const BookNotInStockError = require('../book/BookNotInStockError');
// const BorrowingHistoryNotFound = require('./BorrowingHistoryNotFound');

class TransactionService {
  constructor(models) {
    this._models = models;
    this._insertTransactionHistory = this._insertTransactionHistory.bind(this);
  }

  _insertTransactionHistory(walletId, amount, type, description) {
    const { Transaction } = this._models;
    const transactionData = {
      walletId,
      amount,
      type,
      description,
      createdAt: new Date(),
      updatedAt: new Date()
    };
    return Transaction.create(transactionData);
  }

  async processTransaction(walletId, amount, type, description) {
    const { Wallet } = this._models;
    if (type === 'TOP UP') {
      await Wallet.addWalletBalance(walletId, amount);
    }
    if (type === 'WITHDRAW') {
      await Wallet.decreaseWalletBalance(walletId, amount);
    }
    const transactionData = await this._insertTransactionHistory(
      walletId,
      amount,
      type,
      description
    );
    return transactionData;
  }
}

module.exports = TransactionService;
