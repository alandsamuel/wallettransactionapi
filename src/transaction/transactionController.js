const AppController = require('../common/appController');
const errorHandler = require('../middlewares/errorHandler');
const schemaValidator = require('../middlewares/schemaValidator');
const { post: postSchemas } = require('./transactionSchema');

class UserController extends AppController {
  constructor(app) {
    super(app);
    this._postUser = this._postUser.bind(this);
  }

  registerRoutes() {
    this._app.use('/transactions', this._router);
    this._router.post('/', schemaValidator(postSchemas), errorHandler(this._postUser));
  }

  async _postUser(req, res) {
    const { transactionService } = this._app.locals.services;
    const {
      walletId, amount, type, description
    } = req.body;
    const transactionRecord = await transactionService.processTransaction(
      walletId,
      amount,
      type,
      description
    );
    return res.status(201).json(transactionRecord);
  }
}

module.exports = UserController;
