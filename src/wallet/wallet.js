const { Model, DataTypes } = require('sequelize');
const InsufficientBalanceError = require('../transaction/InsufficientBalanceError');

class Wallet extends Model {
  static init(sequelize) {
    return super.init({
      userId: {
        type: DataTypes.INTEGER,
        field: 'user_id'
      },
      balance: DataTypes.INTEGER
    }, {
      sequelize,
      modelName: 'wallet',
      tableName: 'wallets',
      freezeTableName: true
    });
  }

  static associate(models) {
    const { Wallet: WalletModel, User } = models;
    WalletModel.belongsTo(User, {
      foreignKey: 'userId'
    });
  }

  static findById(walletId) {
    return Wallet.findOne({ where: { id: walletId } });
  }

  static addWalletBalance(walletId, amount) {
    return Wallet.increment('balance', { by: amount, where: { id: walletId } });
  }

  static async decreaseWalletBalance(walletId, amount) {
    const wallet = await Wallet.findById(walletId);
    if (wallet.balance < amount) {
      throw new InsufficientBalanceError();
    }
    return Wallet.decrement('balance', { by: amount, where: { id: walletId } });
  }
}

module.exports = Wallet;
