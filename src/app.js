require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');

const errorMiddleware = require('./middlewares/errorMiddleware');
const User = require('./user/user');
const Wallet = require('./wallet/wallet');
const Transaction = require('./transaction/transaction');
const AppController = require('./common/appController');
const TransactionController = require('./transaction/transactionController');
const TransactionService = require('./transaction/transactionService');
const { connect } = require('./lib/database');

const app = express();
const db = connect();

const createControllers = () => [
  new AppController(app),
  new TransactionController(app)
];

const createModels = () => ({
  User: User.init(db),
  Wallet: Wallet.init(db),
  Transaction: Transaction.init(db)
  // initializing models
});

const initializeModelAssosiations = (models) => {
  const Models = Object.values(models);
  Models.forEach((Model) => {
    if (Model.associate) {
      Model.associate(models);
    }
  });
};

const createServices = models => ({
  transactionService: new TransactionService(models)
  // for initializing services
});

const initializeControllers = () => {
  const controllers = createControllers();
  controllers.forEach((controller) => {
    controller.registerRoutes();
  });
};

const registerDependencies = () => {
  const models = createModels();
  app.locals.models = models;
  app.locals.services = createServices(models);
};

registerDependencies();
initializeModelAssosiations(app.locals.models);

app.use(bodyParser.json());
initializeControllers();
app.use(errorMiddleware);

module.exports = app;
