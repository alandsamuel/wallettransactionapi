const Server = require('./src/server');
const app = require('./src/app');
const config = require('./config');

const server = new Server(app, config);

server.start();

process.on('SIGTERM', () => server.stop());
process.on('SIGINT', () => server.stop());
