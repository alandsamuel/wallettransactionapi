module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.addColumn(
      'wallets', // name of Source model
      'user_id', // name of the key we're adding
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'users', // name of Target model
          key: 'id' // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      }
    ),

  down: queryInterface =>
    queryInterface.removeColumn(
      'wallets', // name of Source model
      'user_id' // key we want to remove
    )
};
