module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.addColumn(
      'transactions', // name of Source model
      'wallet_id', // name of the key we're adding
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'wallets', // name of Target model
          key: 'id' // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      }
    ),

  down: queryInterface =>
    queryInterface.removeColumn(
      'transactions', // name of Source model
      'wallet_id' // key we want to remove
    )
};
