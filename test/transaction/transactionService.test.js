const TransactionService = require('../../src/transaction/transactionService');

describe('TransactionService', () => {
  let transactionService;
  let models;
  let wallet;
  let addWalletBalance;
  let decreaseWalletBalance;
  let create;

  beforeEach(() => {
    addWalletBalance = jest.fn();
    decreaseWalletBalance = jest.fn();
    create = jest.fn();
    wallet = {
      id: 1,
      userId: 1,
      balance: 300000
    };
    models = {
      Wallet: {
        findById: jest.fn().mockResolvedValue(wallet),
        addWalletBalance,
        decreaseWalletBalance
      },
      Transaction: {
        create
      }
    };
    transactionService = new TransactionService(models);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('#processTransaction', () => {
    it('should return transaction history with top up type', async () => {
      const returnValue = {
        walletId: wallet.id,
        amount: 20000,
        type: 'TOP UP',
        description: ' '
      };
      const expectedReturn = {
        walletId: wallet.id,
        amount: 20000,
        type: 'TOP UP',
        description: ' '
      };
      create.mockResolvedValue(returnValue);

      const createTransaction = await transactionService.processTransaction(
        wallet.id,
        20000,
        'TOP UP',
        ' '
      );

      expect(createTransaction).toEqual(expectedReturn);
      expect(addWalletBalance).toHaveBeenCalledTimes(1);
    });

    it('should return transaction history with WITHDRAW type', async () => {
      const returnValue = {
        walletId: wallet.id,
        amount: 20000,
        type: 'WITHDRAW',
        description: ' '
      };
      const expectedReturn = {
        walletId: wallet.id,
        amount: 20000,
        type: 'WITHDRAW',
        description: ' '
      };
      create.mockResolvedValue(returnValue);

      const createTransaction = await transactionService.processTransaction(
        wallet.id,
        20000,
        'WITHDRAW',
        ' '
      );

      expect(createTransaction).toEqual(expectedReturn);
      expect(decreaseWalletBalance).toHaveBeenCalledTimes(1);
    });
  });
});
