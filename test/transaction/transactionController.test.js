const supertest = require('supertest');
const MockDate = require('mockdate');

const app = require('../../src/app');

const {
  models: {
    Wallet, User, Transaction
  }
} = app.locals;

const user = {
  name: 'Surti'
};

const initialBalance = 5000000;

const createWallet = userId => ({
  userId,
  balance: initialBalance
});

const createPayloadTopUp = walletId => ({
  walletId,
  amount: 30000,
  type: 'TOP UP',
  description: ' '
});

const createPayloadWithdraw = walletId => ({
  walletId,
  amount: 50000,
  type: 'WITHDRAW',
  description: ' '
});

const truncateTable = async () => {
  await Transaction.destroy({ truncate: true, cascade: true });
  await Wallet.destroy({ truncate: true, cascade: true });
  await User.destroy({ truncate: true, cascade: true });
};

describe('Transaction Controller', () => {
  const now = '2011-01-01T00:00:00.000Z';
  let request;
  let createdUser;
  let createdWallet;
  let walletId;

  beforeEach(async () => {
    await truncateTable();
    request = supertest(app);
    MockDate.set(now);
    createdUser = await User.create(user);
    createdWallet = await Wallet.create(createWallet(createdUser.id));
    walletId = createdWallet.id;
  });

  afterEach(async () => {
    MockDate.reset();
  });

  describe('POST /transactions', () => {
    it('should return 201 and balance added 30000 when type TOP UP', async () => {
      const payload = createPayloadTopUp(walletId);
      const expectedBalace = initialBalance + payload.amount;
      const expectedResponse = {
        walletId,
        amount: 30000,
        type: 'TOP UP',
        description: ' ',
        createdAt: now,
        updatedAt: now
      };

      const { body } = await request
        .post('/transactions')
        .send(payload)
        .expect(201);

      const addedBalance = await Wallet.findById(walletId);

      expect(body).toMatchObject(expectedResponse);
      expect(addedBalance.balance).toBe(expectedBalace);
    });

    it('should return 201 and balance decrease by 50000 when type WITHDRAW', async () => {
      const payload = createPayloadWithdraw(walletId);
      const expectedBalace = initialBalance - payload.amount;
      const expectedResponse = {
        walletId,
        amount: 50000,
        type: 'WITHDRAW',
        description: ' ',
        createdAt: now,
        updatedAt: now
      };

      const { body } = await request
        .post('/transactions')
        .send(payload)
        .expect(201);

      const decreasedBalance = await Wallet.findById(walletId);

      expect(body).toMatchObject(expectedResponse);
      expect(decreasedBalance.balance).toBe(expectedBalace);
    });

    it('should return 403 when type WITHDRAW and wallet balance 5000000 and amount 6000000', async () => {
      const payload = {
        walletId,
        amount: 6000000,
        type: 'WITHDRAW',
        description: ' '
      };
      const expectedResponse = {
        statusCode: 403,
        code: 'Forbidden',
        message: 'Insufficient balance'
      };

      const { body } = await request
        .post('/transactions')
        .send(payload)
        .expect(403);

      expect(body).toMatchObject(expectedResponse);
    });
  });
});
